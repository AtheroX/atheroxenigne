#version 400 core

in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector[4];
in vec3 toCameraVector;
in float visibility;
in vec4 shadowCoords;

out vec4 out_Color;

uniform sampler2D textureSampler;
uniform vec3 lightColor[4];
uniform vec3 attenuation[4];
uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColor;

uniform sampler2D shadowMap;
uniform float shadowMapSize;

const int pcfShadowCount = 2; // 1 = 3*3, 2 = 5*5, etc
const float totalPixels = (pcfShadowCount * 2.0 + 1.0) * (pcfShadowCount * 2.0 + 1.0);
const float maxShadowFactor = 0.4;

/*CelShading
const float levelsOfCelShading = 3;*/
void main(void){

	float pixelSize = 1.0 / shadowMapSize;
	float total = 0.0;

	for (int x = -pcfShadowCount; x <= pcfShadowCount; x++) {
		for (int y = -pcfShadowCount; y <= pcfShadowCount; y++) {
			float objectNearestLight = texture(shadowMap, shadowCoords.xy + vec2(x, y) * pixelSize).r;
			if(shadowCoords.z > objectNearestLight + 0.005){
				total += 1.0-maxShadowFactor;
			}
		}
	}

	total /= totalPixels;
	float lightFactor = 1.0 - (total * shadowCoords.w);

	vec3 unitNormal = normalize(surfaceNormal);
	vec3 unitVectorToCamera = normalize(toCameraVector);
	
	vec3 totalDiffuse = vec3(0.0);
	vec3 totalSpecular = vec3(0.0);
	
	for(int i = 0; i < 4; i++){
		float distance = length(toLightVector[i]);
		float attFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);
		vec3 unitLightVector = normalize(toLightVector[i]);	
		float nDotl = dot(unitNormal,unitLightVector);
		float brightness = max(nDotl,0.0);
		/*CelShading
		float level = floor(brightness * levelsOfCelShading);
		brightness = level / levelsOfCelShading;*/

		vec3 lightDirection = -unitLightVector;
		vec3 reflectedLightDirection = reflect(lightDirection,unitNormal);
		float specularFactor = dot(reflectedLightDirection , unitVectorToCamera);
		specularFactor = max(specularFactor,0.0);
		float dampedFactor = pow(specularFactor,shineDamper);
		/*CelShading
		float level2 = floor(dampedFactor * levelsOfCelShading);
		dampedFactor = level2 / levelsOfCelShading;*/

		totalDiffuse = totalDiffuse + (brightness * lightColor[i]) / attFactor;
		totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColor[i]) / attFactor;
	}
	totalDiffuse = max(totalDiffuse , maxShadowFactor)* lightFactor; //max shadow
	
	
	vec4 textureColor = texture(textureSampler,pass_textureCoords);
	if(textureColor.a<0.5){
		discard;
	}

	out_Color = vec4(totalDiffuse,1.0) * textureColor + vec4(totalSpecular,1.0);
	out_Color = mix(vec4(skyColor,1.0),out_Color,visibility );
}
