package fontMeshCreator;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import fontRendering.TextMaster;

public class GUIText {

	private String textString;
	private float fontSize;

	private int textMeshVao;
	private int vertexCount;
	private Vector3f color = new Vector3f(0f, 0f, 0f);

	private Vector2f position;
	private float lineMaxSize;
	private int numberOfLines;

	private FontType font;

	private boolean centerText = false;

	private float charWidth = 0.5f;
	private float charEdge = 0.1f;
	private float borderWidth = 0.0f;
	private float borderEdge = 0.4f;
	private Vector2f offset = new Vector2f(0f, 0f);
	private Vector3f outlineColor = new Vector3f(0f, 0f, 0f);
	

	public GUIText(String text, float fontSize, FontType font, Vector2f position, float maxLineLength,
			boolean centered) {
		this.textString = text;
		this.fontSize = fontSize;
		this.font = font;
		this.position = position;
		this.lineMaxSize = maxLineLength;
		this.centerText = centered;
		updateText();
	}
	public void updateText() {
		TextMaster.loadText(this);
	}
	
	public void remove() {
		TextMaster.removetext(this);
	}

	public FontType getFont() {
		return font;
	}

	public void setColor(float r, float g, float b) {
		color.set(r, g, b);
	}

	public void setWidth(float charWidth, float charEdge) {
		this.charWidth = charWidth;
		this.charEdge = charEdge;
	}

	public void setBorder(float borderWidth, float borderEdge) {
		this.borderWidth = borderWidth;
		this.borderEdge = borderEdge;
	}

	public void setOffset(float x, float y) {
		offset.set(x, y);
	}

	public void setOutlineColor(float r, float g, float b) {
		outlineColor.set(r, g, b);
	}

	public Vector3f getColor() {
		return color;
	}

	public float getWidth() {
		return charWidth;
	}

	public float getEdge() {
		return charEdge;
	}

	public float getBorderWidth() {
		return borderWidth;
	}

	public float getBorderEdge() {
		return borderEdge;
	}

	public Vector2f getOffset() {
		return offset;
	}

	public int getNumberOfLines() {
		return numberOfLines;
	}

	public Vector2f getPosition() {
		return position;
	}

	public Vector3f getOutlineColor() {
		return outlineColor;
	}

	public int getMesh() {
		return textMeshVao;
	}

	public void setMeshInfo(int vao, int verticesCount) {
		this.textMeshVao = vao;
		this.vertexCount = verticesCount;
	}

	public int getVertexCount() {
		return this.vertexCount;
	}

	protected float getFontSize() {
		return fontSize;
	}

	protected void setNumberOfLines(int number) {
		this.numberOfLines = number;
	}

	protected boolean isCentered() {
		return centerText;
	}

	protected float getMaxLineSize() {
		return lineMaxSize;
	}

	protected String getTextString() {
		return textString;
	}

}
