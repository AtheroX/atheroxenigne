package toolBox;

import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import entities.Camera;
import entities.Player;
import terrains.Terrain;

public class MousePicker {
	
	// Clase que contiene todos los pasos para llegar de Viewport Space a World Space
	// http://antongerdelan.net/opengl/raycasting.html
	
	private static final int RECURSION_COUNT = 200;
	private static final float RAY_RANGE = 600;

	private Vector3f currentRay = new Vector3f();
	
	private Matrix4f viewMatrix;
	private Matrix4f projectionMatrix;
	private Camera camera;
	
	private List<Terrain> terrain;
	private Vector3f currentTerrainPoint;

	public MousePicker(Camera cam, Matrix4f projection, List<Terrain> terrain) {
		this.camera = cam;
		this.projectionMatrix = projection;
		this.viewMatrix = Maths.createViewMatrix(cam);
		this.terrain = terrain;

	}
	
	public Vector3f getCurrentTerrainPoint() {
		return currentTerrainPoint;
	}

	public Vector3f getCurrentRay() {
		return currentRay;
	}
	
	public void update() {
		viewMatrix = Maths.createViewMatrix(camera);
		currentRay = calculateMouseRay();
		if (intersectionInRange(0, RAY_RANGE, currentRay)) {
			currentTerrainPoint = binarySearch(0, 0, RAY_RANGE, currentRay);
		} else {
			currentTerrainPoint = null;
		}
	}

	private Vector3f calculateMouseRay() {
		float mouseX = Mouse.getX();
		float mouseY = Mouse.getY();
		Vector2f normalizedCoords = getNormalizedCoords(mouseX, mouseY);
		Vector4f homogeneousClipCoords = new Vector4f(normalizedCoords.x, normalizedCoords.y, -1f, 1f); // No hace falta quitar la prespectiva, por eso la z es -1, apunta a la camara
		Vector4f eyeCoords = toEyeCoords(homogeneousClipCoords);
		Vector3f worldRay = toWorldCoords(eyeCoords);
		return worldRay;
	}

	private Vector2f getNormalizedCoords(float mouseX, float mouseY) {
		float x = (2f*mouseX) / Display.getWidth() - 1f;
		float y = (2f*mouseY) / Display.getHeight() - 1f;
		return new Vector2f(x,y); //Como 0,0 es desde abajo a la izquierda es y, si fuese desde arriba a la izquierda ser�a -y
	}
	
	private Vector4f toEyeCoords(Vector4f homogeneousClipCoords) {
		Matrix4f inveterdProjection = Matrix4f.invert(projectionMatrix, null); // El null viene de que se pueden invertir 2 a la vez, pero no es necesaria
		Vector4f eyeCoords = Matrix4f.transform(inveterdProjection, homogeneousClipCoords, null);
		return new Vector4f(eyeCoords.x, eyeCoords.y, -1f, 0f); // Again, la z es -1 para apuntar a la camara
	}
	
	private Vector3f toWorldCoords(Vector4f eyeCoords) {
		Matrix4f invertedViewMatrix = Matrix4f.invert(viewMatrix, null);
		Vector4f rayToWorld = Matrix4f.transform(invertedViewMatrix, eyeCoords, null);
		Vector3f mouseRay = new Vector3f(rayToWorld.x, rayToWorld.y, rayToWorld.z);
		mouseRay.normalise();
		return mouseRay;
	}
	
	private Vector3f getPointOnRay(Vector3f ray, float distance) {
		Vector3f camPos = camera.getPosition();
		Vector3f start = new Vector3f(camPos.x, camPos.y, camPos.z);
		Vector3f scaledRay = new Vector3f(ray.x * distance, ray.y * distance, ray.z * distance);
		return Vector3f.add(start, scaledRay, null);
	}
	
	private Vector3f binarySearch(int count, float start, float finish, Vector3f ray) {
		float half = start + ((finish - start) / 2f);
		if (count >= RECURSION_COUNT) {
			Vector3f endPoint = getPointOnRay(ray, half);
			List<Terrain> terrain = getTerrain(endPoint.getX(), endPoint.getZ());
			if (terrain != null) {
				return endPoint;
			} else {
				return null;
			}
		}
		if (intersectionInRange(start, half, ray)) {
			return binarySearch(count + 1, start, half, ray);
		} else {
			return binarySearch(count + 1, half, finish, ray);
		}
	}

	private boolean intersectionInRange(float start, float finish, Vector3f ray) {
		Vector3f startPoint = getPointOnRay(ray, start);
		Vector3f endPoint = getPointOnRay(ray, finish);
		if (!isUnderGround(startPoint) && isUnderGround(endPoint)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isUnderGround(Vector3f testPoint) {
		List<Terrain> terrains = getTerrain(testPoint.getX(), testPoint.getZ());
		float height = 0;
		for(Terrain terrain:terrains) {
			//Detecta en que terreno esta el player
			if(terrain.getX() <= Player.instance.getPosition().x) { 
				if(terrain.getX() + Terrain.getSize() > Player.instance.getPosition().x) {
					if(terrain.getZ() <= Player.instance.getPosition().z) {
						if(terrain.getZ() + Terrain.getSize() > Player.instance.getPosition().z) {
							if (terrain != null) {
								height = terrain.getHeightOfTerrain(testPoint.getX(), testPoint.getZ());
								
							}
						}
					}
				}
			}
		}
		
		
		if (testPoint.y < height) {
			return true;
		} else {
			return false;
		}
	}

	private List<Terrain> getTerrain(float worldX, float worldZ) {
		return terrain;
	}

}
