package textures;

public class ModelTexture {
	
	// Clase que contiene toda la informacion de las texturas en los modelos

	private int textureID;
	private int normalMap;
	
	private float shineDamper = 1;
	private float reflectivity = 0;
	
	private boolean hasTransparency = false;
	private boolean fakeLightning = false;
	
	//Por si tiene textureAtlas
	private int numberOfRows = 1;
	
	
	public ModelTexture(int id) {
		this.textureID = id;
	}
	
	
	public int getNormalMap() {
		return normalMap;
	}


	public void setNormalMap(int normalMap) {
		this.normalMap = normalMap;
	}


	public int getNumberOfRows() {
		return numberOfRows;
	}


	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}


	public int getID() {
		return this.textureID;
	}
	
	
	public boolean hasTransparency() {
		return hasTransparency;
	}

	
	public void setHasTransparency(boolean hasTransparency) {
		this.hasTransparency = hasTransparency;
	}
	
	
	public boolean isUsingFakeLightning() {
		return fakeLightning;
	}

	
	public void setFakeLightning(boolean fakeLightning) {
		this.fakeLightning = fakeLightning;
	}

	
	public float getShineDamper() {
		return shineDamper;
	}

	
	public void setShineDamper(float shineDamper) {
		this.shineDamper = shineDamper;
	}

	
	public float getReflectivity() {
		return reflectivity;
	}

	
	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
}
