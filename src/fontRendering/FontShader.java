package fontRendering;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import shaders.ShaderProgram;

public class FontShader extends ShaderProgram{

	private static final String VERTEX_FILE = "src/fontRendering/fontVertex.glsl";
	private static final String FRAGMENT_FILE = "src/fontRendering/fontFragment.glsl";
	
	private int location_color;
	private int location_translation;
	private int location_charWidth;
	private int location_charEdge;
	private int location_borderWidth;
	private int location_borderEdge;
	private int location_offset;
	private int location_outlineColor;
	
	public FontShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void getAllUniformLocations() {
		location_color = super.getUniformLocation("color");
		location_translation = super.getUniformLocation("translation");
		location_charWidth = super.getUniformLocation("charWidth");
		location_charEdge = super.getUniformLocation("charEdge");
		location_borderWidth = super.getUniformLocation("borderWidth");
		location_borderEdge = super.getUniformLocation("borderEdge");
		location_offset = super.getUniformLocation("offset");
		location_outlineColor = super.getUniformLocation("outlineColor");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttributes(0, "position");
		super.bindAttributes(1, "textureCoords");
	}

	protected void loadColor(Vector3f color) {
		super.load3DVector(location_color, color);
	}
	protected void loadTranslation(Vector2f translation) {
		super.load2DVector(location_translation, translation);
	}
	protected void loadWidth(float width, float edge) {
		super.loadFloat(location_charWidth, width);
		super.loadFloat(location_charEdge, edge);
	}
	protected void loadBorder(float width, float edge) {
		super.loadFloat(location_borderWidth, width);
		super.loadFloat(location_borderEdge, edge);
	}
	protected void loadOffset(Vector2f offset) {
		super.load2DVector(location_offset, offset);
	}
	protected void loadOutlineColor(Vector3f color) {
		super.load3DVector(location_outlineColor, color);
	}
}
