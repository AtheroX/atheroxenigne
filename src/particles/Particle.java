package particles;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import entities.Player;
import renderEngine.DisplayManager;

public class Particle {

	private Vector3f position;
	private Vector3f velocity;
	private float gravityEffect;
	private float lifeTime;
	private float rotation;
	private float scale;
	
	private ParticleTexture texture;

	private Vector2f texture1Offset = new Vector2f();
	private Vector2f texture2Offset = new Vector2f();
	private float blendFactor;
	
	private float elapsedTime = 0; // Actual lifetime of this particle
	
	private float distanceToCamera; // To sort the particle
	
	private Vector3f updateChange = new Vector3f();

	public Particle(ParticleTexture texture, Vector3f position, Vector3f velocity, float gravity, float lifeTime, float rotation, float scale) {
		super();
		this.texture = texture;
		this.position = position;
		this.velocity = velocity;
		this.gravityEffect = gravity;
		this.lifeTime = lifeTime;
		this.rotation = rotation;
		this.scale = scale;
		ParticleMaster.addParticle(this);
	}

	protected float getDistanceToCamera() {
		return distanceToCamera;
	}

	protected Vector2f getTexture1Offset() {
		return texture1Offset;
	}

	protected Vector2f getTexture2Offset() {
		return texture2Offset;
	}

	protected float getBlendFactor() {
		return blendFactor;
	}

	protected ParticleTexture getTexture() {
		return texture;
	}

	protected Vector3f getPosition() {
		return position;
	}

	protected float getRotation() {
		return rotation;
	}

	protected float getScale() {
		return scale;
	}
	
	/**
	 * Updates this particle
	 * @return if the particle should be alive or not
	 */
	protected boolean update(Camera camera) {
		velocity.y += Player.GRAVITY * gravityEffect * DisplayManager.getTimedeltaTime();
		updateChange.set(velocity);
		updateChange.scale(DisplayManager.getTimedeltaTime());
		Vector3f.add(updateChange, position, position);
		distanceToCamera = Vector3f.sub(camera.getPosition(), position, null).lengthSquared();
		updateTextureCoords();
		elapsedTime += DisplayManager.getTimedeltaTime();
		return elapsedTime < lifeTime;
	}
	
	private void updateTextureCoords() {
		float lifeFactor = elapsedTime / lifeTime;
		int stageCount = texture.getNumberOfRows() * texture.getNumberOfRows();
		float atlasProgression = lifeFactor * stageCount; //Float number, decimal part is the stage, decimal is the blendfactor
		int index1 = (int) Math.floor(atlasProgression);
		int index2 = index1 < stageCount - 1 ? index1+1:index1;
		this.blendFactor = atlasProgression%1;
		setTextureOffset(texture1Offset, index1);
		setTextureOffset(texture2Offset, index2);
	}
	
	private void setTextureOffset(Vector2f offset, int index) {
		int column = index % texture.getNumberOfRows();
		int row = index / texture.getNumberOfRows();
		offset.x = (float) column / texture.getNumberOfRows();
		offset.y = (float) row / texture.getNumberOfRows();
	}
}
