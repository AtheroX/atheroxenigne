#version 140

in vec2 position;
in mat4 modelViewMatrix;
in vec4 textureOffset;
in float blendingFactor;

out vec2 textureCoords1;
out vec2 textureCoords2;
out float blendFactor;

uniform mat4 projectionMatrix;
uniform float numberOfRows;

void main(void){

	vec2 textureCoords = position + vec2(0.5, 0.5);
	textureCoords.y = 1.0 - textureCoords.y;
	textureCoords /= numberOfRows;
	textureCoords1 = textureCoords + textureOffset.xy;
	textureCoords2 = textureCoords + textureOffset.zw;
	blendFactor = blendingFactor;

	gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 0.0, 1.0);

}
