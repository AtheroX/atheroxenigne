package skybox;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import models.RawModel;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;

public class SkyboxRenderer {

private static final float SIZE = 1250f;
	
	// Renderer del skybox, contiene el ciclo dia/noche y el fog de este ciclo

	//{{ Posiciones del cubo
	private static final float[] VERTICES = {        
	    -SIZE,  SIZE, -SIZE,
	    -SIZE, -SIZE, -SIZE,
	    SIZE, -SIZE, -SIZE,
	     SIZE, -SIZE, -SIZE,
	     SIZE,  SIZE, -SIZE,
	    -SIZE,  SIZE, -SIZE,

	    -SIZE, -SIZE,  SIZE,
	    -SIZE, -SIZE, -SIZE,
	    -SIZE,  SIZE, -SIZE,
	    -SIZE,  SIZE, -SIZE,
	    -SIZE,  SIZE,  SIZE,
	    -SIZE, -SIZE,  SIZE,

	     SIZE, -SIZE, -SIZE,
	     SIZE, -SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE, -SIZE,
	     SIZE, -SIZE, -SIZE,

	    -SIZE, -SIZE,  SIZE,
	    -SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE, -SIZE,  SIZE,
	    -SIZE, -SIZE,  SIZE,

	    -SIZE,  SIZE, -SIZE,
	     SIZE,  SIZE, -SIZE,
	     SIZE,  SIZE,  SIZE,
	     SIZE,  SIZE,  SIZE,
	    -SIZE,  SIZE,  SIZE,
	    -SIZE,  SIZE, -SIZE,

	    -SIZE, -SIZE, -SIZE,
	    -SIZE, -SIZE,  SIZE,
	     SIZE, -SIZE, -SIZE,
	     SIZE, -SIZE, -SIZE,
	    -SIZE, -SIZE,  SIZE,
	     SIZE, -SIZE,  SIZE
	};
	//}}
	
	// order de los archivos: derecha, izquierda, arriba, abajo, atras, adelante
	private static String[] TEXTURE_FILES = {"sea/sea0","sea/sea1","sea/sea2","sea/sea3","sea/sea4","sea/sea5"};
	private static String[] NIGHT_TEXTURE_FILES = {"night/night0","night/night1","night/night2","night/night3","night/night4","night/night5"};
	
	private RawModel cube;
	private int texture;
	private int nightTexture;
	private SkyboxShader shader;
	public static float time = 4000, hourFactor = 50, fovFactor = 30f*hourFactor;
	
	public SkyboxRenderer(Loader loader, Matrix4f projectionMatrix) {
		cube = loader.loadToVAO(VERTICES, 3);
		texture = loader.loadCubeMap(TEXTURE_FILES);
		nightTexture = loader.loadCubeMap(NIGHT_TEXTURE_FILES);
		shader = new SkyboxShader();
		shader.start();
		shader.connectTextureUnits();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}
	
	public void render(Camera camera, float r, float g, float b) {
		//GL11.glDepthMask(false);
		//GL11.glDepthRange(1f, 1f);
		shader.start();
		shader.loadViewMatrix(camera);
		shader.loadFogColor(r, g, b);
		GL30.glBindVertexArray(cube.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		bindTextures();
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, cube.getVertexCount());
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		shader.stop();
		//GL11.glDepthRange(0f, 1f);
		//GL11.glDepthMask(true);		
	}
	
	// Funcion encargada de ponerle las texturas al skybox, pero actualmente se encarga del ciclo dia/noche
	private void bindTextures(){
		
	//{{ Tiempo dia/noche
		time += DisplayManager.getTimedeltaTime() * hourFactor;
		time %= 24000;
		int texture1;
		int texture2;
		float blendFactor;
		//System.out.println(time);
		if(time >= 0 && time < 5000){
			texture1 = nightTexture;
			texture2 = nightTexture;
			blendFactor = (time - 0)/(5000 - 0);
			
		}else if(time >= 5000 && time < 8000){
			texture1 = nightTexture;
			texture2 = texture;
			blendFactor = (time - 5000)/(8000 - 5000);
			MasterRenderer.red = MasterRenderer.red + (0.741f-MasterRenderer.red) * (((time - 5000)/(8000 - 5000))/fovFactor);
			MasterRenderer.green = MasterRenderer.green + (0.925f-MasterRenderer.green) * (((time - 5000)/(8000 - 5000))/fovFactor);
			MasterRenderer.blue = MasterRenderer.blue + (0.996f-MasterRenderer.blue) * (((time - 5000)/(8000 - 5000))/fovFactor);
		
			
		}else if(time >= 8000 && time < 21000){
			texture1 = texture;
			texture2 = texture;
			blendFactor = (time - 8000)/(21000 - 8000);

		}else{
			texture1 = texture;
			texture2 = nightTexture;
			blendFactor = (time - 21000)/(24000 - 21000);
			MasterRenderer.red = MasterRenderer.red + (0.078f-MasterRenderer.red) * (((time - 21000)/(24000 - 21000))/fovFactor);
			MasterRenderer.green = MasterRenderer.green + (0.137f-MasterRenderer.green) * (((time - 21000)/(24000 - 21000))/fovFactor);
			MasterRenderer.blue = MasterRenderer.blue + (0.239f-MasterRenderer.blue) * (((time - 21000)/(24000 - 21000))/fovFactor);
			
		}
		
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, texture1);
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, texture2);
		shader.loadBlendFactor(blendFactor);
	//}}
	}
}
