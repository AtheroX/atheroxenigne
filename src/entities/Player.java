package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import renderEngine.DisplayManager;
import terrains.Terrain;

public class Player extends Entity {
	
	private static final float RUN_SPEED = 30;
	private static final float ROT_SPEED = 180;
	public static final float GRAVITY = -90;
	private static final float JUMP_POWER = 50;
		
	private float currentSpeed = 0;
	private float rotSpeed = 0;
	private float upwardsSpeed = 0;	
	
	private float runSpeed = 1f;
	
	private boolean onAir = false;
	
	public static Player instance;
	
	public static Vector2f lastTerrain = new Vector2f(3,3);
	
	public Player(TexturedModel model, Vector3f position, Vector3f rotation, Vector3f scale) {
		super(model, position, rotation, scale);
		instance = this;
	}
	
	public void move(Terrain terrain) {
		checkInputs();
		
		//Rotacion
		super.rotate(0, rotSpeed * DisplayManager.getTimedeltaTime(), 0);
		
		//Translacion
		float distance = currentSpeed * runSpeed * DisplayManager.getTimedeltaTime();
		float dx = (float) (distance * Math.sin(Math.toRadians(super.getRotY())));
		float dz = (float) (distance * Math.cos(Math.toRadians(super.getRotY())));
		super.move(dx, 0, dz);
		
		//Salto
		upwardsSpeed += GRAVITY * DisplayManager.getTimedeltaTime();
		super.move(0, upwardsSpeed * DisplayManager.getTimedeltaTime(), 0);
		float terrainHeight = terrain.getHeightOfTerrain(super.getPosition().x, super.getPosition().z);
		if(super.getPosition().y < terrainHeight) {
			upwardsSpeed = 0;
			onAir = false;
			super.getPosition().y = terrainHeight;
		}
	}
	
	private void jump() {
		if(!onAir) {
			this.upwardsSpeed = JUMP_POWER;
			onAir = true;
		}
	}
	
	
	// Comprueba los inputs
	private void checkInputs() {
		
		// Adelante atras
		if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
			this.currentSpeed = RUN_SPEED;
		}else if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
			this.currentSpeed = -RUN_SPEED;
		}else {
			this.currentSpeed = 0;
		}
		
		// Derecha izquierda
		if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
			this.rotSpeed = -ROT_SPEED;
		}else if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
			this.rotSpeed = ROT_SPEED;
		}else {
			this.rotSpeed = 0;
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			runSpeed = 8f;
		}else {
			runSpeed = 1f;
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			jump();
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_M)) {
			System.out.println(getPosition());
		}
	}

	public float getUpwardsSpeed() {
		return upwardsSpeed;
	}

	public boolean isOnAir() {
		return onAir;
	}
	
	
}
