package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.util.Log;

/**
 * Camera controller class
 * 
 * @author AtheroX
 * 
 */

public class Camera {
	
	private Vector3f cameraPreset = new Vector3f(0,0,0);
	private float distanceToPlayer = 24;
	private float angleAroundPlayer = 0;
	private float verticalOffset = 6.25f;

	
	private Vector3f position = new Vector3f(0,0,0);
	private float pitch = 14; 	// Up-Down rotation
	private float yaw;			// Left-Right rotation
	private float roll;			// Do a barrel roll
	
	private Player player;
	
	private boolean isFirstPerson = false;
	private boolean freeRotate = false;
	
	public Camera(Player player) {
		this.player = player;
		createCameraPreset();
	}
	
	//{{ GETTERS
		/**
		 * @return Returns the actual position of camera
		 */
		public Vector3f getPosition() {
			return position;
		}
		/**
		 * @return Returns the up-down rotation of camera
		 */
		public float getPitch() {
			return pitch;
		}
		
		/**
		 * @return Returns the up-down rotation of camera but inverted
		 */
		public void invertPitch() {
			this.pitch = -pitch;
		}

		/**
		 * @return Returns the Left-Right rotation of camera
		 */
		public float getYaw() {
			return yaw;
		}

		/**
		 * @return Returns the rotation on itself of camera
		 */
		public float getRoll() {
			return roll;
		}
	//}}
		
	/**
	 * If this is called the camera moves
	 * <hr>
	 * {@code if Keycode.G is pressed, the actual configuration is sysoed} <br>
	 * {@code if Keycode.Z is pressed, the actual configuration is saved} <br>
	 * {@code if Keycode.R is pressed, the actual configuration is reseted to Z.Configuration}
	 */
	public void move() {
		if(Keyboard.isKeyDown(Keyboard.KEY_R)) {
			resetCameraPosition();
		}else if(Keyboard.isKeyDown(Keyboard.KEY_G)) {
			Log.debug(distanceToPlayer + " : " + " : " + verticalOffset + " : " + pitch);
		}else if(Keyboard.isKeyDown(Keyboard.KEY_Z)) {
			createCameraPreset();
		}
		
		calculateZoom();
		calculatePitch();
		
		if(!isFirstPerson) {
			calculateAngleAroundPlayer();
		}else {
			angleAroundPlayer = 0f;
		}
		
		calculateVerticalOffset();

		float horizontalDistance = calculateHorizontalDistance();
		float verticalDistance = calculateVerticalDistance();
		calculateCameraPosition(horizontalDistance, verticalDistance);
		
		if(freeRotate)
			angleAroundPlayer+=0.25f;
		
		this.yaw = 180 - (player.getRotY() + angleAroundPlayer);	
	}
	
	/**
	 * Puts the camera in it's position using the player's position and calculating the
	 * necesary offset
	 * <hr>
	 * @param horizDistance Float - Horizontal distance from player
	 * @param vertiDistance Float - Vertical distance from player
	 */
	private void calculateCameraPosition(float horizDistance, float vertiDistance) {
		float theta = player.getRotY() + angleAroundPlayer;
		float offsetX = (float) (horizDistance * Math.sin(Math.toRadians(theta)));
		float offsetZ = (float) (horizDistance * Math.cos(Math.toRadians(theta)));
		position.x = player.getPosition().x - offsetX;
		position.z = player.getPosition().z - offsetZ;
		position.y = player.getPosition().y + vertiDistance + verticalOffset;
	}
	
	
	/**
	 * Calculates the horizontal distance to player using it's position and cosinus
	 * @return Horizontal Distance to player
	 */
	private float calculateHorizontalDistance() {
		return (float) (distanceToPlayer * Math.cos(Math.toRadians(pitch)));
	}
	/**
	 * Calculates the vertical distance to player using it's position and sinus
	 * @return Vertical Distance to player
	 */
	private float calculateVerticalDistance() {
		return (float) (distanceToPlayer * Math.sin(Math.toRadians(pitch)));
	}
	
	/**
	 * Sets the zoom. It's clamped from 0 to 72<br>
	 * Also, if zoom is 0 set's the camera to first person
	 */
	private void calculateZoom() {
		float zoomLevel = Mouse.getDWheel() * 0.05f;
		distanceToPlayer -= zoomLevel;
		
		if(distanceToPlayer>372f) {
			distanceToPlayer = 372f;
		}else if(distanceToPlayer<=0f){
			distanceToPlayer = 0f;
			isFirstPerson = true;
		}else {
			isFirstPerson = false;
		}
	}
	
	/**
	 * Gets the Right-Click and changes the pitch in base of that<br>
	 * Contains a Finite state machine
	 */
	private void calculatePitch() {
		if(Mouse.isButtonDown(1)) {
			float pitchChange = Mouse.getDY() * 0.1f;
			pitch -= pitchChange;
		}
		/**
		 * Finite state machine for the lower pitch you get by actual zoom
		 *  TODO Use an exponetial algorithm for this like 2^x
		 */
		switch (""+distanceToPlayer) {
			case "72.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 20f){
					pitch = 20f;
				}
				break;

			case "66.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 18f){
					pitch = 18f;
				}

			case "60.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 16f){
					pitch = 16f;
				}
				
			case "54.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 14f){
					pitch = 14f;
				}
				
			case "48.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 12f){
					pitch = 12f;
				}
				
			case "42.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 10f){
					pitch = 10f;
				}
				
			case "36.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 8f){
					pitch = 8f;
				}
				
			case "30.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 6f){
					pitch = 6f;
				}
				
			case "24.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < 4f){
					pitch = 4f;
				}
				
			case "18.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < -5f){
					pitch = -5f;
				}
				
			case "12.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < -18f){
					pitch = -18f;
				}
				
			case "6.0":
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < -30f){
					pitch = -30f;
				}

			default:
				if(pitch > 70f) {
					pitch = 70f;
				}else if(pitch < -45f){
					pitch = -45f;
				}
				break;
		}
	}
	
	/**
	 * While the Left-Click is pressed, camera rotates arround player in base of mouse's horizontal moves
	 */
	private void calculateAngleAroundPlayer() {
		if(Mouse.isButtonDown(0)) {
			float angleChange = Mouse.getDX() * 0.3f;
			angleAroundPlayer -= angleChange;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_F))
			freeRotate = !freeRotate;
	}
	
	/**
	 * While the Scroll-Click is pressed, the camera's Y-offset changes in base of mouse's vertical moves
	 */
	private void calculateVerticalOffset() {
		if(Mouse.isButtonDown(2)) {
			verticalOffset += Mouse.getDY() * 0.05f;
		}
		
		if(verticalOffset>6.5f) {
			verticalOffset = 6.5f;
		}else if(verticalOffset<2f){
			verticalOffset = 2f;
		}
	}
	
	/**
	 * Resets the camera position to the saved one (Keycode.R)
	 */
	private void resetCameraPosition() {
		angleAroundPlayer = cameraPreset.x;
		verticalOffset = cameraPreset.y;
		pitch = cameraPreset.z;
	}
	
	/**
	 * Saves the actual comera position (Keycode.Z)
	 */
	private void createCameraPreset() {
		cameraPreset = new Vector3f(angleAroundPlayer,verticalOffset,pitch);
	}
}
