package entities;

import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import toolBox.Maths;

/**
 * This class has main necessary things for a entity, like the 
 * 
 * @author AtheroX
 * 
 */

public class Entity {

	private TexturedModel model;
	private Vector3f position;
	private Vector3f rotation;
	private Vector3f scale;
	
	// Texture atlas index
	private int textureIndex = 0;
	
	/**
	 * Constructor without ATLAS texture
	 * @param model 		The model of the entity
	 * @param position		The Transform.Position of the entity
	 */
	public Entity(TexturedModel model, Vector3f position, Vector3f rotation, Vector3f scale) {
		this.model = model;
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
	}
	
	/**
	 * Constructor with ATLAS texture
	 * @param model 		The model of the entity
	 * @param position		The Transform.Position of the entity
	 */	
	public Entity(TexturedModel model, int index, Vector3f position, Vector3f rotation, Vector3f scale)  {
		this.model = model;
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
		this.textureIndex = index;
	}

	public float getTextureXOffset() {
		int column = textureIndex % model.getTexture().getNumberOfRows();
		return (float) column / (float) model.getTexture().getNumberOfRows();
	}

	public float getTextureYOffset() {
		int row = textureIndex / model.getTexture().getNumberOfRows();
		return (float) row / (float) model.getTexture().getNumberOfRows();
	}
	
	public void move(float dx, float dy, float dz) {
		this.position.x += dx;
		this.position.y += dy;
		this.position.z += dz;
	}
	
	public void rotate(float dx, float dy, float dz) {
		this.rotation.x += dx;
		this.rotation.y += dy;
		this.rotation.y = Maths.clamp(this.rotation.y, -361, 361);
		if(this.rotation.y > 360)
			this.rotation.y = 0;
		else if(this.rotation.y < -360)
			this.rotation.y = 0;
		this.rotation.z += dz;
	}
	
	public void scale(float dx, float dy, float dz) {
		this.scale.x += dx;
		this.scale.y += dy;
		this.scale.z += dz;
	}

	public TexturedModel getModel() {
		return model;
	}

	public void setModel(TexturedModel model) {
		this.model = model;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getRotX() {
		return rotation.x;
	}

	public void setRotX(float rotX) {
		this.rotation.x = rotX;
	}

	public float getRotY() {
		return rotation.y;
	}

	public void setRotY(float rotY) {
		this.rotation.y = rotY;
	}

	public float getRotZ() {
		return rotation.z;
	}

	public void setRotZ(float rotZ) {
		this.rotation.z = rotZ;
	}

	public float getScaleX() {
		return scale.x;
	}

	public void setScaleX(float scaleX) {
		this.scale.x = scaleX;
	}

	public float getScaleY() {
		return scale.y;
	}

	public void setScaleY(float scaleY) {
		this.scale.y = scaleY;
	}

	public float getScaleZ() {
		return scale.z;
	}

	public void setScaleZ(float scaleZ) {
		this.scale.z = scaleZ;
	}
	
	
}
