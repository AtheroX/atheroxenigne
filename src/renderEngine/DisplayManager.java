package renderEngine;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.PixelFormat;


public class DisplayManager {

	/* Clase encargada de crear la ventana del juego
	 * Llevar el tiempo de la ventana abierta
	 * Limite de FPS
	 */
	
	private static final int WIDTH = 1280;
	private static final int HEIGHT = 720;
	private static final int FPS_CAP = 120;
	public static final String VERSION = "0.9.6";
	private static final int MSAA_SAMPLES = 4; //Antialiasing

	private static long lastFrameTime;
	private static float delta;
	
	// fpsNum is an aux to calc the fpsRate, lastFPS is the last frame updated and frameRate is the quantity of frames of last second
	public static float fpsNum, lastFPS;
	public static int frameRate;

	// Crea la ventana del juego con sus atributos, como su ancho/alto, nombre, uso de pixeles, etc.
	public static void createDisplay() {
		ContextAttribs attribs = new ContextAttribs(3,3).withForwardCompatible(true).withProfileCore(true);
		try {
			Display.setDisplayMode(new DisplayMode(WIDTH,HEIGHT));
			Display.create(new PixelFormat().withSamples(MSAA_SAMPLES), attribs);
			GL11.glEnable(GL13.GL_MULTISAMPLE);
			Display.setResizable(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		GL11.glViewport(0, 0, WIDTH, HEIGHT);
		lastFrameTime = getCurrentTime();
		lastFPS = Sys.getTime();
	}
	
	// Actualiza la pantalla hasta FPS_CAP veces por segundo y calcula el tiempo que se tard� en renderizar el ultimo frame
	public static void updateDisplay() {
		Display.sync(FPS_CAP);
		Display.update();
		long currentFrameTime = getCurrentTime();
		delta = (currentFrameTime - lastFrameTime)/1000f;
		calcFrameRate();
		lastFrameTime = currentFrameTime;
	}
	
	private static void calcFrameRate() {
	    if (Sys.getTime() - lastFPS > 1000) { //ONCE A SECOND
	    	frameRate = Math.round(fpsNum);
			Display.setTitle("AtheroX Engine "+VERSION+"        "+frameRate);
	    	fpsNum = 0; //reset the FPS counter
	        lastFPS += 1000; //add one second
	    }
	    fpsNum++;
	    

//		long start = System.currentTimeMillis();
//		long end = System.currentTimeMillis();
//		if(end-start != 0)
//			DisplayManager.veces+=end-start;
	}

	public static float getTimedeltaTime() {
		return delta;
	}
	
	// Cierra la ventana
	public static void closeDisplay() {
		Display.destroy();
	}
	
	// Coje el tiempo del ordenador en milisegundos
	public static long getCurrentTime() {
		return Sys.getTime()*1000/Sys.getTimerResolution();
	}
}
