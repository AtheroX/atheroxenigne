package engineTester;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.Player;
import fontMeshCreator.FontType;
import fontMeshCreator.GUIText;
import fontRendering.TextMaster;
import guis.GuiRenderer;
import guis.GuiTexture;
import models.TexturedModel;
import normalMappingObjConverter.NormalMappedObjLoader;
import objConverter.OBJFileLoader;
import particles.ParticleMaster;
import particles.ParticleSystem;
import particles.ParticleTexture;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;
import terrains.Terrain;
import textures.ModelTexture;
import textures.TerrainTexture;
import textures.TerrainBlendmap;
import toolBox.Maths;
import toolBox.MousePicker;
import water.WaterFrameBuffers;
import water.WaterRenderer;
import water.WaterShader;
import water.WaterTile;

/**
 * The core, this contains every entity that will be in the game
 * 
 * @author AtheroX<br>
 * 
 */
public class MainGameLoop {
		
	public static void main(String[] args) {

	//{{ ---- STARTERS ----
		DisplayManager.createDisplay();
		Loader loader = new Loader();
		Random random = new Random();
		GuiRenderer guiRenderer = new GuiRenderer(loader);
		List<Terrain> terrains = new ArrayList<Terrain>();
		List<Entity> entities = new ArrayList<Entity>();
		List<Entity> normalMapEntities = new ArrayList<Entity>();
		List<Light> lights = new ArrayList<Light>();
		TextMaster.init(loader);
		//{{ ---- PLAYER & CAMERA ----
			TexturedModel playerStaticModel = new TexturedModel(OBJFileLoader.loadOBJ("testcharSmooth", loader), new ModelTexture(loader.loadTexture("testchar")));

			Player player = new Player(playerStaticModel, new Vector3f(Terrain.getSize()*3+Terrain.getSize()/2, 0, Terrain.getSize()*3+Terrain.getSize()/2), new Vector3f(0, 0, 0), new Vector3f(1, 1, 1));
			Camera camera = new Camera(player);	
		//}}
		MasterRenderer renderer = new MasterRenderer(loader, camera);
		ParticleMaster.init(loader, renderer.getProjectionMatrix());
		

		boolean needsToRender = true, firstJoin = true;;
		Vector2f movedToTerrain = new Vector2f();
		
	//}} --- --- --- --- --- ---

	//{{ ---- LIGHTS ----
//		Light sun = new Light(new Vector3f(500,10000,0), new Vector3f(.75f,.8f,.9f));
//		lights.add(sun);
//		lights.add(new Light(new Vector3f( 10000,10000, 10000), new Vector3f(.2f,.2f,.2f)));
//		lights.add(new Light(new Vector3f(-10000,10000,-10000), new Vector3f(.2f,.2f,.2f)));
		
		Light sun = new Light(new Vector3f(-1000000,2500000,-1000000), new Vector3f(1.2f,1.2f,1.2f));
		lights.add(sun);
		
	//}} --- --- --- --- --- ---
			
	//{{ ---- TERRAINS ----
		
		//{{ ---- TEXTURES ----

		TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grassFloor"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("stonePath"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("sand"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("clay"));
	
		//}}
		//{{ ---- BLENDMAPS ----

		TerrainBlendmap texturePack = new TerrainBlendmap(backgroundTexture, rTexture, gTexture, bTexture);
		//TerrainTexture blendMapFeo =  new TerrainTexture(loader.loadBlendMap("feo"));
		TerrainTexture grassBlendMap =  new TerrainTexture(loader.loadBlendMap("LakeMap"));
		
		//}}
		//{{ ---- TERRAINS ----

		
//		terrains.add(new Terrain(0, 0, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(0, 1, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(0, 2, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(1, 0, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(1, 1, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(1, 2, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(2, 0, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(2, 1, loader, texturePack, grassBlendMap, "heightmap4"));
//		terrains.add(new Terrain(2, 2, loader, texturePack, grassBlendMap, "heightmap4"));
		
		/* Other terrains
			Terrain terrain1 = new Terrain(0,0,loader, texturePack, blendMapFeo, "heightmap3");
			terrains.add(terrain1);
			terrains.add(new Terrain(0,-1,loader, texturePack, blendMapFeo, "heightmap3"));
			Terrains.add(new Terrain(-1,0,loader, texturePack, blendMapFeo, "heightmap3"));
			terrains.add(new Terrain(-1,-1,loader, texturePack, blendMapFeo, "heightmap3"));
			*/
		
		//}}
		//{{ ---- MOUSE PICKER ----

		MousePicker mousePicker = new MousePicker(camera, renderer.getProjectionMatrix(), terrains);
		
		//}}
		
	//}} --- --- --- --- --- ---	
		
	//{{ ---- WATERS ----
		WaterFrameBuffers fbos = new WaterFrameBuffers();

		int waterHeight = -15;
		List<WaterTile> waters = new ArrayList<WaterTile>();
		WaterShader waterShader = new WaterShader();
		WaterRenderer waterRenderer = new WaterRenderer(loader, waterShader, renderer.getProjectionMatrix(), fbos, "lake1DUDV", "lakeNormal");

		//TODO the waters are overlapping
		for (Terrain terrain : terrains) {
			waters.add(new WaterTile(WaterTile.TILE_SIZE+terrain.getX(), WaterTile.TILE_SIZE+terrain.getZ(), waterHeight));

		}
		
	//}} --- --- --- --- --- ---	

	//{{ ---- MODELS ----
		//{{ ---- MOD. Crate ---- 
		
		TexturedModel crateStaticModel = new TexturedModel(OBJFileLoader.loadOBJ("crate", loader), new ModelTexture(loader.loadTexture("woodenplank")));
		Entity crateEntity = new Entity(crateStaticModel, new Vector3f(-30, 1, -40), new Vector3f(0, 0, 0), new Vector3f(4, 4, 4));
		entities.add(crateEntity);
		
		//}}
		//{{ ---- MOD. Crate2 ----
		
		TexturedModel crate2StaticModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("box", loader), new ModelTexture(loader.loadTexture("woodenplank")));
		crate2StaticModel.getTexture().setNormalMap(loader.loadTexture("normalmaps/woodenplankNormal"));
		crate2StaticModel.getTexture().setShineDamper(.1f);
		crate2StaticModel.getTexture().setReflectivity(0.1f);
		Entity crate2Entity = new Entity(crate2StaticModel, new Vector3f(-30, 1, -40), new Vector3f(0, 0, 0), new Vector3f(4, 4, 4));
		normalMapEntities.add(crate2Entity);
		
		//}}
		/*{{ ---- MOD. Tree01 ---- 
		
		ModelData tree01Data = OBJFileLoader.loadOBJ("tree01");
		RawModel tree01Model = loader.loadToVAO(tree01Data.getVertices(), tree01Data.getTextureCoords(), tree01Data.getNormals(), tree01Data.getIndices());
		TexturedModel tree01StaticModel = new TexturedModel(tree01Model, new ModelTexture(loader.loadTexture("tree01")));
		
		for(Terrain terrain:terrains) {
			for (int entityAmount = 0; entityAmount < 50; entityAmount++) {
				float x = random.nextInt((int) Terrain.getSize()) + terrain.getX();
				float z = random.nextInt((int) Terrain.getSize()) + terrain.getZ();
				float y = terrain.getHeightOfTerrain(x, z);
				if(y<waterHeight+5)
					continue;
				entities.add(new Entity(tree01StaticModel, new Vector3f(x,y,z), 0, random.nextFloat()*360, 0, 2, 2, 2));	
			}
		}
		}}*/
		//{{ ---- MOD. Tree02 ---- 
		
		TexturedModel tree02StaticModel = new TexturedModel(OBJFileLoader.loadOBJ("tree02-2", loader), new ModelTexture(loader.loadTexture("tree02")));
		
		for(Terrain terrain:terrains) {
			for (int entityAmount = 0; entityAmount < 50*Terrain.getSize()/800; entityAmount++) {
				float x = random.nextInt((int) Terrain.getSize()) + terrain.getX();
				float z = random.nextInt((int) Terrain.getSize()) + terrain.getZ();
				float y = terrain.getHeightOfTerrain(x, z)-7f;
				if(y<waterHeight+5)
					continue;
				entities.add(new Entity(tree02StaticModel, new Vector3f(x,y,z), new Vector3f(0, random.nextFloat()*360, 0), new Vector3f(1.3f, 1.3f, 1.3f)));
			}
		}
		
		//}}
		//{{ ---- MOD. Tree02-2 ----
		
		TexturedModel tree02_2StaticModel = new TexturedModel(OBJFileLoader.loadOBJ("tree02-3", loader), new ModelTexture(loader.loadTexture("tree02")));
		
		for(Terrain terrain:terrains) {
			for (int entityAmount = 0; entityAmount < 50*Terrain.getSize()/800; entityAmount++) {
				float x = random.nextInt((int) Terrain.getSize()) + terrain.getX();
				float z = random.nextInt((int) Terrain.getSize()) + terrain.getZ();
				float y = terrain.getHeightOfTerrain(x, z)-7f;
				if(y<waterHeight+5)
					continue;
				entities.add(new Entity(tree02_2StaticModel, new Vector3f(x,y,z), new Vector3f(0, random.nextFloat()*360, 0), new Vector3f(1.3f, 1.3f, 1.3f)));
			}
		}
		
		//}}
		//{{ ---- MOD. Fern ----
		ModelTexture fernTexture = new ModelTexture(loader.loadTexture("fern"));
		fernTexture.setNumberOfRows(2);
		TexturedModel fernStaticModel = new TexturedModel(OBJFileLoader.loadOBJ("fern", loader), fernTexture);
		fernStaticModel.getTexture().setHasTransparency(true);
		fernStaticModel.getTexture().setFakeLightning(true);
		
		for(Terrain terrain:terrains) {
			for (int entityAmount = 0; entityAmount < 300*Terrain.getSize()/800; entityAmount++) {
				float x = random.nextInt((int) Terrain.getSize()) + terrain.getX();
				float z = random.nextInt((int) Terrain.getSize()) + terrain.getZ();
				float y = terrain.getHeightOfTerrain(x, z);
				float r = Maths.randomFloat(0.2f, 1.5f);
				if(y<waterHeight+5)
					continue;
				entities.add(new Entity(fernStaticModel, random.nextInt(4), new Vector3f(x,y,z), new Vector3f(0, random.nextFloat()*360, 0), new Vector3f(r, r, r)));	
			}
		}
		//}}	
		//{{ ---- MOD. Grass ----
		
		TexturedModel grassStaticModel = new TexturedModel(OBJFileLoader.loadOBJ("grass", loader), new ModelTexture(loader.loadTexture("grass")));
		grassStaticModel.getTexture().setHasTransparency(true);
		grassStaticModel.getTexture().setFakeLightning(true);
		
		for(Terrain terrain:terrains) {
			for (int entityAmount = 0; entityAmount < 2000*Terrain.getSize()/800; entityAmount++) {
				float x = random.nextInt((int) Terrain.getSize()) + terrain.getX();
				float z = random.nextInt((int) Terrain.getSize()) + terrain.getZ();
				float y = terrain.getHeightOfTerrain(x, z);
				if(y<waterHeight+5)
					continue;
				entities.add(new Entity(grassStaticModel, new Vector3f(x,y,z), new Vector3f(0, random.nextFloat()*360, 0), new Vector3f(3, 3, 3)));	
			}		
		}
		
		//}}
	//}} --- --- --- --- --- ---
		
	//{{ ---- GUI ----
		List<GuiTexture> guis = new ArrayList<GuiTexture>();
		GuiTexture logo = new GuiTexture(loader.loadTexture("atheroX"), new Vector2f(-1f+0.09f, 1f-0.16f), new Vector2f(0.09f, 0.16f));
		guis.add(logo);
		//{{ ---- FONTS & TEXTS ----
			FontType sans = new FontType(loader.loadTextTexture("sans2"), new File("res/texture/fonts/sans2.fnt"));
			//FontType candara = new FontType(loader.loadTextTexture("candara"), new File("res/texture/fonts/candara.fnt"));

			GUIText atheroxText = new GUIText("AtheroX Engine v"+DisplayManager.VERSION, 3f, sans, new Vector2f(0.292f,-0.002f), 1f, true);
			atheroxText.setColor(1, .1f, .2f);
			atheroxText.setBorder(.7f, 0.1f);
			atheroxText.setWidth(0.5f, 0.1f);
			
		//}}
		
		/* Reflection and refraction GUI textures
		GuiTexture reflection = new GuiTexture(fbos.getReflectionTexture(), new Vector2f(.25f,.75f), new Vector2f(.25f,.25f));
		GuiTexture refraction = new GuiTexture(fbos.getRefractionTexture(), new Vector2f(.75f,.75f), new Vector2f(.25f,.25f));
		guis.add(reflection);
		guis.add(refraction);
		*/
			
	//}}--- --- --- --- --- ---
		
	//{{ ---- PARTICLES ----

		ParticleTexture pTexture_redSphere = new ParticleTexture(loader.loadParticleTexture("redSphere"), 4, true);
//		ParticleTexture pTexture_fire = new ParticleTexture(loader.loadParticleTexture("fire"), 8, true);
		ParticleSystem simpleSys = new ParticleSystem(pTexture_redSphere, 5000, 10, 0.01f, 2, 0.2f);
//		ParticleSystem simpleSys2 = new ParticleSystem(pTexture_fire, 5000, 10f, .15f, 1, 20f);

	//}}--- --- --- --- --- ---
		
	//{{ ---- LOOP ----
		while(!Display.isCloseRequested()) {
		//{{ ---- PLAYER MOVES ----
			
			for(Terrain terrain:terrains) {
				// Player's ground terrain
				if(terrain.getX() <= player.getPosition().x) { 
					if(terrain.getX() + Terrain.getSize() > player.getPosition().x) {
						if(terrain.getZ() <= player.getPosition().z) {
							if(terrain.getZ() + Terrain.getSize() > player.getPosition().z) {
								player.move(terrain);								
								Vector2f actualTerrain = new Vector2f(terrain.getX()/Terrain.getSize(), terrain.getZ()/Terrain.getSize());
								if(!Player.lastTerrain.equals(actualTerrain)) {
									needsToRender = true;
									Vector2f.sub(actualTerrain, Player.lastTerrain, movedToTerrain);
									Player.lastTerrain = actualTerrain;
								}
								continue;
							}
						}
					}
				}
			}
			camera.move();
		//}}
		//{{ ---- MOUSEPICK ----

//			mousePicker.update();
//			Vector3f terrainPoint = mousePicker.getCurrentTerrainPoint();
//			if(terrainPoint != null) {
//				if(Keyboard.isKeyDown(Keyboard.KEY_C)) {
//					crateEntity.setPosition(terrainPoint);
//				}
//				if(Keyboard.isKeyDown(Keyboard.KEY_V)) {
//					crate2Entity.setPosition(terrainPoint);
//				}
//			}
		//}}
		//{{ ---- PARTICLES ----
			//JumpEffect
			if(player.isOnAir() && player.getUpwardsSpeed() > 0)
				simpleSys.generateParticles(new Vector3f(player.getPosition()));
			ParticleMaster.update(camera);

		//}}
		//{{ ---- RENDER (Normal & Water) ----

			//{{ Shadows
			renderer.renderShadowMap(entities, sun);
			//}}
			GL11.glEnable(GL30.GL_CLIP_DISTANCE0);

			//{{ Reflection Render 
				// Requires of a inverted position of camera (position 5 > -5 and yaw 45 > -45)
			fbos.bindReflectionFrameBuffer();
			float distance = 2 * (camera.getPosition().y - waterHeight);
			
			camera.getPosition().y -= distance;
			camera.invertPitch();
			
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, 1, 0, -waterHeight+0.5f));
			renderer.renderEntity(player);
			
			camera.getPosition().y += distance;
			camera.invertPitch();
			//}}
			//{{ Refraction Render 
			fbos.bindRefractionFrameBuffer();
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, -1, 0, waterHeight));
			renderer.renderEntity(player);

			fbos.unbindCurrentFrameBuffer();
			//}}
			//{{ Terrain Generation Render
			if(needsToRender) {
				//terrains.clear();
				int terrX = (int)(Player.lastTerrain.x);
				int terrZ = (int)(Player.lastTerrain.y);

				Terrain upleft = new Terrain(terrX-1, terrZ+1, loader, texturePack, grassBlendMap);
				Terrain up = 	new Terrain(terrX, terrZ+1, loader, texturePack, grassBlendMap);
				Terrain upright = new Terrain(terrX+1, terrZ+1, loader, texturePack, grassBlendMap);
				
				Terrain left = new Terrain(terrX-1, terrZ, loader, texturePack, grassBlendMap);
				Terrain center = new Terrain(terrX, terrZ, loader, texturePack, grassBlendMap);
				Terrain right = new Terrain(terrX+1, terrZ, loader, texturePack, grassBlendMap);
				
				Terrain downleft = new Terrain(terrX-1, terrZ-1, loader, texturePack, grassBlendMap);
				Terrain down = 	new Terrain(terrX, terrZ-1, loader, texturePack, grassBlendMap);
				Terrain downright = new Terrain(terrX+1, terrZ-1, loader, texturePack, grassBlendMap);


				Terrain eupleft = new Terrain(terrX-1, terrZ+2, loader, texturePack, grassBlendMap);
				Terrain eup = new Terrain(terrX, terrZ+2, loader, texturePack, grassBlendMap);
				Terrain eupright = new Terrain(terrX+1, terrZ+2, loader, texturePack, grassBlendMap);

				Terrain eleftup = new Terrain(terrX-2, terrZ+1, loader, texturePack, grassBlendMap);
				Terrain eleft = new Terrain(terrX-2, terrZ, loader, texturePack, grassBlendMap);
				Terrain eleftdown = new Terrain(terrX-2, terrZ-1, loader, texturePack, grassBlendMap);

				Terrain erightup = new Terrain(terrX+2, terrZ+1, loader, texturePack, grassBlendMap);
				Terrain eright = new Terrain(terrX+2, terrZ, loader, texturePack, grassBlendMap);
				Terrain erightdown = new Terrain(terrX+2, terrZ-1, loader, texturePack, grassBlendMap);

				Terrain edownleft = new Terrain(terrX-1, terrZ-2, loader, texturePack, grassBlendMap);
				Terrain edown = new Terrain(terrX, terrZ-2, loader, texturePack, grassBlendMap);
				Terrain edownright = new Terrain(terrX+1, terrZ-2, loader, texturePack, grassBlendMap);
				
				Terrain bupleft = new Terrain(terrX-2, terrZ+2, loader, texturePack, grassBlendMap);
				Terrain bupright = new Terrain(terrX+2, terrZ+2, loader, texturePack, grassBlendMap);
				Terrain bdownleft = new Terrain(terrX-2, terrZ-2, loader, texturePack, grassBlendMap);
				Terrain bdownright = new Terrain(terrX+2, terrZ-2, loader, texturePack, grassBlendMap);
				
				for (Terrain t: terrains) {
					System.out.println(t.getX()/Terrain.getSize()+":"+t.getZ()/Terrain.getSize());
				}System.out.println();	
				
				if(firstJoin) {
					terrains.add(upleft);
					terrains.add(up);
					terrains.add(upright);
					terrains.add(left);
					terrains.add(center);
					terrains.add(right);
					terrains.add(downleft);
					terrains.add(down);
					terrains.add(downright);
					for (Terrain t : terrains) {
						waters.add(new WaterTile(WaterTile.TILE_SIZE+t.getX(), WaterTile.TILE_SIZE+t.getZ(), waterHeight));
						t.setPopulated(true);
					}
					firstJoin = false;
					
				}else if(movedToTerrain.equals(new Vector2f(0f,-1f))) {
					for (int i = 0; i < terrains.size(); i++) {
						if(terrains.get(i).getX()/Terrain.getSize() == eupright.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == eupright.getZ()/Terrain.getSize()) {
							terrains.remove(i); 
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == eup.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == eup.getZ()/Terrain.getSize()) {
							terrains.remove(i); 
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == eupleft.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == eupleft.getZ()/Terrain.getSize()) {
							terrains.remove(i); 
							waters.remove(i);
							i--;
						}
					}
										
					terrains.add(downleft);
					terrains.add(down);
					terrains.add(downright);
				}else if(movedToTerrain.equals(new Vector2f(0f,1f))) {
					
					for (int i = 0; i < terrains.size(); i++) {
						if(terrains.get(i).getX()/Terrain.getSize() == edownright.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == edownright.getZ()/Terrain.getSize()) {
							terrains.remove(i); 
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == edown.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == edown.getZ()/Terrain.getSize()) {
							terrains.remove(i);
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == edownleft.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == edownleft.getZ()/Terrain.getSize()) {
							terrains.remove(i); 
							waters.remove(i);
							i--;
						}
					}
										
					terrains.add(upleft);
					terrains.add(up);
					terrains.add(upright);
				}else if(movedToTerrain.equals(new Vector2f(-1f,0f))) {
					
					for (int i = 0; i < terrains.size(); i++) {
						if(terrains.get(i).getX()/Terrain.getSize() == erightup.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == erightup.getZ()/Terrain.getSize()) {
							terrains.remove(i);
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == eright.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == eright.getZ()/Terrain.getSize()) {
							terrains.remove(i);
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == erightdown.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == erightdown.getZ()/Terrain.getSize()) {
							terrains.remove(i);
							waters.remove(i);
							i--;
						}
					}
										
					terrains.add(upleft);
					terrains.add(left);
					terrains.add(downleft);
				}else if(movedToTerrain.equals(new Vector2f(1f,0f))) {
					
					for (int i = 0; i < terrains.size(); i++) {
						if(terrains.get(i).getX()/Terrain.getSize() == eleftup.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == eleftup.getZ()/Terrain.getSize()) {
							terrains.remove(i);
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == eleft.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == eleft.getZ()/Terrain.getSize()) {
							terrains.remove(i);
							waters.remove(i);
							i--;
						}else if(terrains.get(i).getX()/Terrain.getSize() == eleftdown.getX()/Terrain.getSize()
								&& terrains.get(i).getZ()/Terrain.getSize() == eleftdown.getZ()/Terrain.getSize()) {
							terrains.remove(i);
							waters.remove(i);
							i--;
						}
					}
										
					terrains.add(upright);
					terrains.add(right);
					terrains.add(downright);
				}
				
				for (int i = 0; i < terrains.size(); i++) {
					if(!terrains.get(i).iAmPopulated) {
						waters.add(new WaterTile(WaterTile.TILE_SIZE+terrains.get(i).getX(), WaterTile.TILE_SIZE+terrains.get(i).getZ(), waterHeight));
						terrains.get(i).setPopulated(true);
					}
					
					if(terrains.get(i).getX()/Terrain.getSize() == bupleft.getX()/Terrain.getSize()
							&& terrains.get(i).getZ()/Terrain.getSize() == bupleft.getZ()/Terrain.getSize()) {
						terrains.remove(i);
						waters.remove(i);
						i--;
					}else if(terrains.get(i).getX()/Terrain.getSize() == bupright.getX()/Terrain.getSize()
							&& terrains.get(i).getZ()/Terrain.getSize() == bupright.getZ()/Terrain.getSize()) {
						terrains.remove(i);
						waters.remove(i);
						i--;
					}else if(terrains.get(i).getX()/Terrain.getSize() == bdownleft.getX()/Terrain.getSize()
							&& terrains.get(i).getZ()/Terrain.getSize() == bdownleft.getZ()/Terrain.getSize()) {
						terrains.remove(i);
						waters.remove(i);
						i--;
					}else if(terrains.get(i).getX()/Terrain.getSize() == bdownright.getX()/Terrain.getSize()
							&& terrains.get(i).getZ()/Terrain.getSize() == bdownright.getZ()/Terrain.getSize()) {
						terrains.remove(i);
						waters.remove(i);
						i--;
					}
				}
				needsToRender = false;
			}
			//}}			
			//{{ Base Render 
			GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, -1, 0, 99999));
			renderer.renderEntity(player);
			waterRenderer.render(waters, camera, sun);

			ParticleMaster.renderParticles(camera);

			guiRenderer.render(guis);

			TextMaster.render();
			
			DisplayManager.updateDisplay();
			//}}


		//}}
			
		}
	//}}
		
	//{{ ---- CLEAN UP ----
		ParticleMaster.cleanUp();
		TextMaster.cleanUp();
		fbos.cleanUp();
		waterShader.cleanUp();
		guiRenderer.cleanUp();
		renderer.cleanUp();
		loader.cleanUp();
		DisplayManager.closeDisplay();
	//}}
	}


}
